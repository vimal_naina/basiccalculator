package com.vimalnaina.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import net.objecthunter.exp4j.ExpressionBuilder
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    lateinit var txtDisplay: TextView

    //To check whether last button pressed is number or not.
    var lastNumeric: Boolean = false

    //To check whether last button pressed is Dot or not.
    var lastDot: Boolean = false

    //To check current state is in error or not.
    var stateError: Boolean = false

    //Initial Zero
    var firstZero:Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        txtDisplay = findViewById(R.id.txtDisplay)
    }

    fun onDigit(view: View) {
        if(stateError || firstZero){
            txtDisplay.text = (view as Button).text
            stateError = false
            firstZero = false
        }else{
            txtDisplay.append((view as Button).text)
        }
        lastNumeric = true
    }

    fun onOperator(view: View) {
        if(lastNumeric && !stateError){
            txtDisplay.append((view as Button).text)
            lastNumeric = false
            lastDot = false
        }
    }

    fun onDot(view: View) {
        if(lastNumeric && !lastDot && !stateError){
            txtDisplay.append(".")
            lastDot = true
            lastNumeric = false
        }
    }

    fun onClearAll(view: View) {
        this.txtDisplay.text = "0"
        lastNumeric = false
        lastDot = false
        stateError = false
        firstZero = true
    }

    fun onEqual(view: View) {
        if(lastNumeric && !stateError){
            val txt = txtDisplay.text.toString()
            val expression = ExpressionBuilder(txt).build()
            try {
                val result = expression.evaluate()
                txtDisplay.text = result.toString()
                lastDot = true
            }catch (e: ArithmeticException){
                txtDisplay.text = "Error"
                stateError = true
                lastNumeric = false
            }catch (e: Exception){
                txtDisplay.text = "Error"
                stateError = true
                lastNumeric = false
            }
        }
    }

    fun onClear(view: View) {
        if(!firstZero){
            if(txtDisplay.text.length>1){
                var rmLastChar = txtDisplay.text.toString()
                txtDisplay.text = rmLastChar.substring(0 , rmLastChar.length-1)
                if(!(txtDisplay.text.last().isDigit())){
                    lastDot = true
                }
            }else{
                txtDisplay.text = "0"
                firstZero = true
            }
        }
    }
}